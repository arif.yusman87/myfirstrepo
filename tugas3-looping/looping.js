console.log("1. LOOPING MENGGUNAKAN WHILE");

let myNumber1 = 2;
let myNumber2 = 20;

console.log("-------------------------------------------------------------------------------------------------------");
console.log("LOOPING PERTAMA");

while ( myNumber1 <= 20) {    
    console.log(`${myNumber1} - I love coding`);
    myNumber1 += 2;
}

console.log("-------------------------------------------------------------------------------------------------------");
console.log("LOOPING KEDUA");

while(myNumber2 >= 2) {
    console.log(`${myNumber2} - I will become a mobile developer`);
    myNumber2 -= 2;
}

console.log("=======================================================================================================");
console.log("2. LOOPING MENGGUNAKAN FOR");
console.log("-------------------------------------------------------------------------------------------------------");

let y = 0;

for (let counter1 = 1; counter1 <= 20; counter1++) {

    if (counter1 % 3 === 0 && counter1 % 2 !== 0) {
        console.log(`${counter1} - I love coding`);
    } else if (counter1 % 2 === 0) {
        console.log(`${counter1} - Berkualitas`);
    } else {
        console.log(`${counter1} - Santai`);
    }

}


console.log("=======================================================================================================");
console.log("3. MEMBUAT PERSEGI PANJANG");
console.log("-------------------------------------------------------------------------------------------------------");

let tagar = "";

for (let i = 1; i < 5; i++) {

    for (let x = 1; x < 9; x++ ) {
        
        tagar += "#";
        
    }

    tagar += "\n";
}
console.log(tagar);


console.log("=======================================================================================================");
console.log("4. MEMBUAT TANGGA");
console.log("-------------------------------------------------------------------------------------------------------");

let tangga = "";

for (let a = 1; a < 8; a++ ) {
    
    tangga += "#";
    console.log(tangga);
    
}


console.log("=======================================================================================================");
console.log("4. MEMBUAT PAPAN CATUR");
console.log("-------------------------------------------------------------------------------------------------------");

let catur = "";
let spasi = "";


for (let y = 1; y < 9; y++) {

    for (let z = 0; z < y; z++) {
        spasi += " ";
    }

    for (let z = 0; z < y; z++) {
        catur += "#";
    }

}

console.log(spasi + catur);