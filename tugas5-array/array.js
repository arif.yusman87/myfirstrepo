console.log("=======================================================================================================");
console.log("SOAL NO. 1 (RANGE)");
console.log("-------------------------------------------------------------------------------------------------------");

function range(startNum, finishNum) {
    let number = [];
    
    if (startNum <= finishNum) {
        for(let i = startNum; i <= finishNum; i++) {
            number.push(i);
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            number.push(i);
        }
    } else if (startNum === 1 || startNum === null || finishNum === null) {
        number.unshift(-1);
    } else {
        number.unshift(-1);
    }

    return number;

}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


console.log("=======================================================================================================");
console.log("SOAL NO. 2 (RANGE WITH STAPE)");
console.log("-------------------------------------------------------------------------------------------------------");

function rangeWithStep (startNum, finishNum, step) {
    let number1 = [];

    if (startNum < finishNum) {
        for (let x = startNum; x <= finishNum; x += step) {
            number1.push(x);
        }
    } else if (startNum > finishNum) {
        for (let x = startNum; x >= finishNum; x -= step) {
            number1.push(x);
        }
    }

    return number1;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
