// A. TUGAS STRING
// 1. if-else

var nama = '';
var peran = '';

// Output untuk Input nama = '' dan peran = ''
// "Nama harus diisi!"
if (nama === '') {
    console.log("Nama harus diisi!");
}

// Output untuk Input nama = 'John' dan peran = ''
// "Halo John, Pilih peranmu untuk memulai game!"
if (nama === '') {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
}

// Output untuk Input nama = 'Jane' dan peran 'Penyihir'
// "Selamat datang di Dunia Werewolf, Jane"
// "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
var nama = 'Jane';
var peran = 'Penyihir';

if (nama === 'Jane' && peran === 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Penyihir " + nama+ ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama === 'Jenita' && peran === 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama === 'Junaedi' && peran === 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
}

// Output untuk Input nama = 'Jenita' dan peran 'Guard'
// "Selamat datang di Dunia Werewolf, Jenita"
// "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
var nama = 'Jenita';
var peran = 'Guard';

if (nama === 'Jane' && peran === 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Penyihir " + nama+ ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama === 'Jenita' && peran === 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama === 'Junaedi' && peran === 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
}

// Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
// "Selamat datang di Dunia Werewolf, Junaedi"
// "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"
var nama = 'Junaedi';
var peran = 'Werewolf';

if (nama === 'Jane' && peran === 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Penyihir " + nama+ ", kamu dapat melihat siapa yang menjadi werewolf!");
} else if (nama === 'Jenita' && peran === 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (nama === 'Junaedi' && peran === 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
}

// ============================================================================================================================================
// 2. SWITCH

var hari = 3;
var bulan = 9;
var tahun = 1987;

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;
    default:
        console.log("Masukkan tanggal lahir anda");
        break;
}

var birthDate = hari + " " + bulan + " " + tahun;
console.log(`Tanggal lahir anda: ${birthDate}`);